#!/usr/bin/env python
import argparse
import sys
import socket
import random
import struct

from scapy.all import sendp, send, get_if_list, get_if_hwaddr
from scapy.all import Packet
from scapy.all import Ether, IP, UDP, TCP


def main():
    print 'Sending UDP packets...'

    # 
    iface = ["veth0", "veth2", "veth4", "veth6"]
    print '########## packet 1 ##########'
    pkt =  Ether(src='00:00:00:00:00:01', dst='00:00:00:00:00:02')
    pkt = pkt /IP(src='10.0.0.1', dst='10.0.0.2') / UDP(dport=1234, sport=random.randint(49152,65535))
    pkt.show2()
    sendp(pkt, iface=iface[0], verbose=False)

    print '\n########## packet 2 ##########'
    pkt =  Ether(src='00:00:00:00:00:01', dst='00:00:00:00:00:04')
    pkt = pkt /IP(src='10.0.0.1', dst='10.0.0.4') / UDP(dport=1234, sport=random.randint(49152,65535))
    pkt.show2()
    sendp(pkt, iface=iface[0], verbose=False)

    print '\n########## packet 3 ##########'
    pkt =  Ether(src='00:00:00:00:00:03', dst='00:00:00:00:00:02')
    pkt = pkt /IP(src='10.0.0.3', dst='10.0.0.2') / UDP(dport=1234, sport=random.randint(49152,65535))
    pkt.show2()
    sendp(pkt, iface=iface[2], verbose=False)

    print '\n########## packet 4 ##########'
    pkt =  Ether(src='00:00:00:00:00:03', dst='00:00:00:00:00:04')
    pkt = pkt /IP(src='10.0.0.3', dst='10.0.0.4') / UDP(dport=1234, sport=random.randint(49152,65535))
    pkt.show2()
    sendp(pkt, iface=iface[2], verbose=False)

    print '\n########## packet 5 ##########'
    pkt =  Ether(src='00:00:00:00:00:01', dst='00:00:00:00:00:03')
    pkt = pkt /IP(src='10.0.0.1', dst='10.0.0.3') / UDP(dport=1234, sport=random.randint(49152,65535))
    pkt.show2()
    sendp(pkt, iface=iface[0], verbose=False)

    print '\n########## packet 6 ##########'
    pkt =  Ether(src='00:00:00:00:00:03', dst='00:00:00:00:00:01')
    pkt = pkt /IP(src='10.0.0.3', dst='10.0.0.1') / UDP(dport=1234, sport=random.randint(49152,65535))
    pkt.show2()
    sendp(pkt, iface=iface[2], verbose=False)

    print '\n########## packet 7 ##########'
    pkt =  Ether(src='00:00:00:00:00:02', dst='00:00:00:00:00:04')
    pkt = pkt /IP(src='10.0.0.2', dst='10.0.0.4') / UDP(dport=1234, sport=random.randint(49152,65535))
    pkt.show2()
    sendp(pkt, iface=iface[1], verbose=False)

    print '\n########## packet 8 ##########'
    pkt =  Ether(src='00:00:00:00:00:04', dst='00:00:00:00:00:02')
    pkt = pkt /IP(src='10.0.0.4', dst='10.0.0.2') / UDP(dport=1234, sport=random.randint(49152,65535))
    pkt.show2()
    sendp(pkt, iface=iface[3], verbose=False)

    print '\n########## packet 9 ##########'
    pkt =  Ether(src='00:00:00:00:00:02', dst='00:00:00:00:00:01')
    pkt = pkt /IP(src='10.0.0.2', dst='10.0.0.1') / UDP(dport=1234, sport=random.randint(49152,65535))
    pkt.show2()
    sendp(pkt, iface=iface[1], verbose=False)

    print '\n########## packet 10 ##########'
    pkt =  Ether(src='00:00:00:00:00:04', dst='00:00:00:00:00:01')
    pkt = pkt /IP(src='10.0.0.4', dst='10.0.0.1') / UDP(dport=1234, sport=random.randint(49152,65535))
    pkt.show2()
    sendp(pkt, iface=iface[3], verbose=False)

    print '\n########## packet 11 ##########'
    pkt =  Ether(src='00:00:00:00:00:02', dst='00:00:00:00:00:03')
    pkt = pkt /IP(src='10.0.0.2', dst='10.0.0.3') / UDP(dport=1234, sport=random.randint(49152,65535))
    pkt.show2()
    sendp(pkt, iface=iface[1], verbose=False)

    print '\n########## packet 12 ##########'
    pkt =  Ether(src='00:00:00:00:00:04', dst='00:00:00:00:00:03')
    pkt = pkt /IP(src='10.0.0.4', dst='10.0.0.3') / UDP(dport=1234, sport=random.randint(49152,65535))
    pkt.show2()
    sendp(pkt, iface=iface[3], verbose=False)


if __name__ == '__main__':
    main()


